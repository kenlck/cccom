json.array!(@contribution_transactions) do |contribution_transaction|
  json.extract! contribution_transaction, :id, :transaction_date, :bank_statement_date, :amount, :user_id
  json.url contribution_transaction_url(contribution_transaction, format: :json)
end
