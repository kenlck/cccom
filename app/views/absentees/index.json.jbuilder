json.array!(@absentees) do |absentee|
  json.extract! absentee, :id, :member_id, :function_date, :remarks
  json.url absentee_url(absentee, format: :json)
end
