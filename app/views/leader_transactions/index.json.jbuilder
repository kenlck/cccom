json.array!(@leader_transactions) do |leader_transaction|
  json.extract! leader_transaction, :id
  json.url leader_transaction_url(leader_transaction, format: :json)
end
