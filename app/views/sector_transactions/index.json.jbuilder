json.array!(@sector_transactions) do |sector_transaction|
  json.extract! sector_transaction, :id
  json.url sector_transaction_url(sector_transaction, format: :json)
end
