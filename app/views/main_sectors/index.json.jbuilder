json.array!(@main_sectors) do |main_sector|
  json.extract! main_sector, :id, :sector_name, :staff_id
  json.url main_sector_url(main_sector, format: :json)
end
