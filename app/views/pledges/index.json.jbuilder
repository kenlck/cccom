json.array!(@pledges) do |pledge|
  json.extract! pledge, :id, :year, :amount, :member_id
  json.url pledge_url(pledge, format: :json)
end
