module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Central Christian Church of Malaysia"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  #change this
  def generate_contribution_transactions_csv(transactions, options = {})
    desired_columns = ["Paid By", "Member", "For which Month?", "Bank statement date", "Bank Account", "Fund Type", "Amount", "Comment", "Created At"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      transactions.each do |transaction|
        paid = transaction.paid_by.common_name
        member = transaction.member.common_name
        month = transaction.transaction_date.strftime("%B")
        bank = transaction.bank_statement_date.strftime("%d/%m/%Y")
        bank_account = transaction.bank_account
        fund_type = ContributionTransaction::FUND_TYPE_NAME[transaction.fund_type]
        amount ='%.2f' % transaction.amount
        comment = transaction.comment
        created_at = transaction.created_at.strftime("%d/%m/%Y")

        csv << [paid, member, month, bank, bank_account, fund_type, amount, comment, created_at]
      end
    end
  end

  def generate_member_pledge_csv(members, options = {})
    desired_columns = ["IC Name", "Common Name", "Sector", "Year", "Monthly Con", "Special Con", "Building Fund", "Comments"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      members.each do |member|
        gender = member.gender ? "Male" : "Female"
        is_leader = member.is_leader ? "Yes" : "No"
        leader_id = member.leader ? member.leader.common_name : ''
        sector_id = member.sector ? member.sector.sector_type : ''
        year = member.pledges.last ? member.pledges.last.year : ''
        amount = member.pledges.last ? member.pledges.last.amount : ''
        scp = member.pledges.last ? member.pledges.last.special_contribution_pledge : ''
        bfp = member.pledges.last ? member.pledges.last.building_fund_pledge : ''
        comment = member.pledges.last ? member.pledges.last.amount_percentage : ''
        csv << [member.ic_name, member.common_name, sector_id, year, amount, scp, bfp, comment]
      end
    end
  end

  def generate_member_csv(members, options = {})
    desired_columns = ["id", "ic_name", "common_name", "notes", "ic_no", "handphone", "is_leader", "gender", "leader_id", "sector_id", "baptism_date", "email_address", "address_1", "address_2", "receipt_name"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      members.each do |member|
        gender = member.gender ? "Male" : "Female"
        is_leader = member.is_leader ? "Yes" : "No"
        leader_id = member.leader ? member.leader.common_name : ''
        sector_id = member.sector ? member.sector.sector_type : ''
        if member.sector.active
          csv << [member.id, member.ic_name, member.common_name, member.notes, member.ic_no, member.handphone, is_leader, gender, leader_id, sector_id, member.baptism_date, member.email, member.current_address_1, member.current_address_2, member.receipt_name]
        end
      end
    end
  end

  def generate_nongivers_csv(members, options = {})
  desired_columns = ["ic_name", "common_name", "notes", "gender", "leader_id", "sector_id", "pledged"]
  CSV.generate(options) do |csv|
    csv << desired_columns
    members.each do |member|
      gender = member.gender ? "Male" : "Female"
      is_leader = member.is_leader ? "Yes" : "No"
      leader_id = member.leader ? member.leader.common_name : ''
      sector_id = member.sector ? member.sector.sector_type : ''
      pledge = member.pledges.last ? member.pledges.last.amount : ''
      if member.sector.active
        csv << [member.ic_name, member.common_name, member.notes, gender, leader_id, sector_id, pledge]
      end
    end
  end
end


  def generate_tpri_csv(cts, options = {})
    desired_columns = ["receipt name", "receipt ic", "bank statement date", "amount", "common name"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      cts.each do |ct|
        receipt_name = ct.receipt_to ? ct.receipt_to.receipt_name : ''
        receipt_ic = ct.receipt_to ? ct.receipt_to.receipt_ic : ''
        common_name = ct.member ? ct.member.common_name : ''
        bank_statement_date = ct.bank_statement_date.strftime("%d/%m/%Y")
        amount = "%.2f" % ct.amount
        csv << [receipt_name, receipt_ic, bank_statement_date, amount, common_name]
      end
    end
  end

  def generate_sector_report(sector_transactions)
    desired_columns = ["", "Pro M", "Pro W", "CHI GYM M", "CHI GYM W", "Y&F M", "Y&F W", "SOS M", "SOS W", "YP M", "YP W", "Campus M", "Campus W"]
    CSV.generate do |csv|
      csv << desired_columns
      csv << ["Baptisms", @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 2, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 2, members: { gender: false }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 7, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 7, members: { gender: false }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 4, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 4, members: { gender: false }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 1, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 1, members: { gender: false }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 6, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 6, members: { gender: false }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 5, members: { gender: true }).count, @sector_transactions.joins(:member).where(previous_sector_id: 13, current_sector_id: 5, members: { gender: false }).count]
      csv << ["Restorations", sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 2, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 2, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 7, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 7, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 4, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 4, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 1, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 1, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 6, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 6, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 5, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 12, current_sector_id: 5, members: { gender: false }).count]
      csv << ["Move In", sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 2, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 2, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 7, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 7, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 4, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 4, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 1, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 1, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 6, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 6, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 5, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 11, current_sector_id: 5, members: { gender: false }).count]
      csv << ["Cancelled Memberships", sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 8, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 8, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 8, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 8, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 8, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 8, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 8, members: { gender: false }).count]
      csv << ["Move Out", sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 10, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 10, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 10, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 10, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 10, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 10, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 10, members: { gender: false }).count]
      csv << ["Deaths", sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 2, current_sector_id: 9, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, current_sector_id: 9, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, current_sector_id: 9, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, current_sector_id: 9, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, current_sector_id: 9, members: { gender: false }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 9, members: { gender: true }).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, current_sector_id: 9, members: { gender: false }).count]
      csv << ["Internal Transfers In", sector_transactions.joins(:member).where(current_sector_id: 2, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 2, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 7, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 7, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 4, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 4, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 1, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 1, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 6, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 6, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 5, members: { gender: true }).where("previous_sector_id NOT IN (?)", [11,12,13]).count,
      sector_transactions.joins(:member).where(current_sector_id: 5, members: { gender: false }).where("previous_sector_id NOT IN (?)", [11,12,13]).count]
      csv << ["Internal Transfers Out", sector_transactions.joins(:member).where(previous_sector_id: 2, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 2, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 7, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 4, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 1, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 6, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, members: { gender: true }).where("current_sector_id NOT IN (?)", [8,9,10]).count,
      sector_transactions.joins(:member).where(previous_sector_id: 5, members: { gender: false }).where("current_sector_id NOT IN (?)", [8,9,10]).count]
      csv<< []
      csv<< ["", "Previous Sector", "Current Sector", "Member", "When?"]
      @sector_transactions.each do |sector_transaction|
      csv << ["", sector_transaction.previous_sector.sector_type, sector_transaction.current_sector.sector_type, sector_transaction.member.common_name, sector_transaction.created_at.strftime("%d/%m/%Y")]
      end
    end
  end
end
