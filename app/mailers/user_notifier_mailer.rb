class UserNotifierMailer < ApplicationMailer
  default :from => 'churchoffice@cccmy.org'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_notification(member, sector_transaction, new_users)
    @member = member
    @sector_transaction = sector_transaction

    mail( :to => new_users,
      :from => 'mingsern@cccmy.org',
      :subject => "You have a new notification. #{@member.common_name} has been transferred to #{@sector_transaction.current_sector.sector_type}" )
  end

  # send a reminder email to the user, pass in the user object that   contains the user's email address
  def send_reminder(member, month)
    @member = member
    @month = month

    mail( :to => @member.email,
      :subject => "You have a SPAM notification. #{@member.common_name}" )
  end
end
