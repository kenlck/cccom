require 'csv'

class Member < ApplicationRecord

  before_save { self.ic_name = ic_name.titleize }
  before_save { self.common_name = common_name.titleize }
  # has_many :contributions, dependent: :destroy
  belongs_to :leader, class_name: 'Member', foreign_key: 'leader_id', optional: true, inverse_of: :members
  belongs_to :member_group_leader, class_name: 'Member', foreign_key: 'member_group_id', optional: true, inverse_of: :members
  belongs_to :previous_leader, class_name: 'Member', foreign_key: 'previous_leader_id', optional: true
  belongs_to :current_leader, class_name: 'Member', foreign_key: 'current_leader_id', optional: true
  belongs_to :sector, -> { order(id: :asc)}

  has_many :contribution_transactions
  has_many :pledges
  has_many :ct_receipt_to, class_name: 'ContributionTransaction', foreign_key: 'receipt_to_id'
  has_many :ct_paid_for, class_name: 'ContributionTransaction', foreign_key: 'paid_by_id'
  has_many :members, class_name: 'Member', foreign_key: 'leader_id', inverse_of: :leader
  has_many :member_groups, class_name: 'Member', foreign_key: 'member_group_id', inverse_of: :member_group_leader
  validates :ic_name, presence: true
  validates :common_name, presence: true
  validates :ic_no, presence: true
  validates :d_o_b, presence: true
  validates :baptism_date, presence: true

  def self.search(search)
    where("common_name ILIKE '%#{search}%' OR ic_name ILIKE '%#{search}%'")
  end

  def self.filter_out_on_behalf()
    where(on_behalf: false)
  end

  def self.search_by_sector(search)
    where(sector: Sector.search(search).first)
  end

  def self.search_by_leader(search)
    where(leader: Member.find_by(id: search))
  end

  def self.options_for_select_type
    order('LOWER(common_name)').map { |e| e.is_leader ? [e.common_name, e.id] : nil }.compact
  end

  def self.query_for_pledge_amount(id)
    a = where(id: id).last
    a.pledges.last ? a.pledges.order(year: :desc).last.amount : nil
  end

  def self.query_for_pledge_percentage(id)
    a = where(id: id).last
    a.pledges.last ? a.pledges.order(year: :desc).last.amount_percentage : nil
  end

  def self.query_for_building_fund(id)
    a = where(id: id).last
    a.pledges.last ? a.pledges.order(year: :desc).last.building_fund_pledge : nil
  end

  def self.query_for_special_contribution(id)
    a = where(id: id).last
    a.pledges.last ? a.pledges.order(year: :desc).last.special_contribution_pledge : nil
  end

  def self.query_for_last_pledge(id)
    a = where(id: id).last
    a.pledges.order(year: :desc).first
  end

  def self.import(file)
    spreadsheet = Roo::Spreadsheet.open(file.path)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      member = find_by(id: row["id"]) || new
      member.attributes = row.to_hash
      member.save!
    end
  end

  def self.to_csv(options = {})
  desired_columns = ["ic_name", "common_name", "ic_no", "handphone", "is_leader", "gender", "leader_id", "sector_id"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      all.each do |member|
        gender = member.gender ? "Male" : "Female"
        is_leader = member.is_leader ? "Yes" : "No"
        leader_id = member.leader ? member.leader.common_name : ''
        sector_id = member.sector ? member.sector.sector_type : ''
        csv << [member.ic_name, member.common_name, member.ic_no, member.handphone, is_leader, gender, leader_id, sector_id]
      end
    end
  end

end
