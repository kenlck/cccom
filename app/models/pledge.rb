class Pledge < ApplicationRecord
  belongs_to :member

  default_scope { order(year: :asc) } #this will affect sector contribution report and pledges list

  def self.query_by_year(query) #sample input "1/1/2017"
    q = query.to_date
    where(year: q.beginning_of_year..q.end_of_year)
  end
end
