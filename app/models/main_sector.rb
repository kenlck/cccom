class MainSector < ApplicationRecord
  belongs_to :staff, optional: true
  has_many :sectors
  has_many :members, through: :sectors
end
