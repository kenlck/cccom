class SectorTransaction < ApplicationRecord
  belongs_to :previous_sector, class_name: 'Sector', foreign_key: 'previous_sector_id'
  belongs_to :current_sector, class_name: 'Sector', foreign_key: 'current_sector_id'
  belongs_to :member
  has_many :members

end
