class Sector < ApplicationRecord
  belongs_to :staff, optional: true
  belongs_to :user, optional: true
  belongs_to :main_sector
  has_many :members
  has_many :contribution_transactions, through: :members
  has_and_belongs_to_many :users

  def self.options_for_select
    order('LOWER(sector_type)').map { |e| [e.sector_type, e.id] }
  end

  def self.options_for_select_type
    order('LOWER(sector_type)').where(new: false, active: true).map { |e| [e.sector_type, e.sector_type] }
  end

  def self.nonactive
    order('LOWER(sector_type)').where(new: false).map { |e| [e.sector_type, e.sector_type] }
  end

  def self.search(search)
    where("sector_type LIKE ?", "%#{search}%")
  end
end
