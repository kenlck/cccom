class ContributionTransaction < ApplicationRecord
  before_save :check_late_pay
  belongs_to :member
  belongs_to :receipt_to, class_name: 'Member'
  belongs_to :paid_by, class_name: 'Member'

  default_scope { order(created_at: :desc) }

  FUND_TYPE = %w[contribution special_contribution building_fund].freeze
  FUND_TYPE_NAME = {
    'contribution' => 'Contribution',
    'special_contribution' => 'Special Contribution',
    'building_fund' =>  'Building Fund'
  }
  BANK_ACCOUNT = %w[TPRI CCCOM].freeze


  attr_accessor :member_ids

  def self.query_by_sector(query)
    joins(member: :sector)
    .where(sectors: { sector_type: query })
  end

  def self.query_by_date_range(query) #sample input "1/8/2017 - 1/9/2017"
    q = query.delete(' ').split('-').map(&:to_date)
    where(transaction_date: q.first..q.last)
  end

  def self.query_by_month(query) #sample input "1/1/2017"
    q = query.to_date
    where(transaction_date: q.beginning_of_month..q.end_of_month)
  end

  def self.query_by_month2(query) #sample input "1/1/2017"
    q = query.to_date
    where(bank_statement_date: q.beginning_of_month..q.end_of_month)
  end

  def self.query_by_year(query) #sample input "1/1/2017"
    q = query.to_date
    where(transaction_date: q.beginning_of_year..q.end_of_year)
  end

  def self.query_by_year2(query) #sample input "1/1/2017"
    q = query.to_date
    where(bank_statement_date: q.beginning_of_year..q.end_of_year)
  end

  def check_late_pay() #sample input "1/1/2017"
    if self.transaction_date.beginning_of_month < self.bank_statement_date.beginning_of_month
      self.late_pay = true
      return
    end
    self.late_pay = false
  end

  def self.to_csv(options = {})
    desired_columns = ["receipt name", "receipt ic", "bank statement date", "amount"]
    CSV.generate(options) do |csv|
      csv << desired_columns
      all.each do |ct|
        receipt_name = ct.member ? ct.member.receipt_name : ''
        receipt_ic = ct.member ? ct.member.receipt_ic : ''
        bank_statement_date = ct.bank_statement_date
        amount = ct.amount
        csv << [receipt_name, receipt_ic, bank_statement_date, amount]
      end
    end
  end
end
