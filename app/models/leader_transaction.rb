class LeaderTransaction < ApplicationRecord
  belongs_to :member
  belongs_to :previous_leader, class_name: 'Member', foreign_key: 'previous_leader_id'
  belongs_to :current_leader, class_name: 'Member', foreign_key: 'current_leader_id'


end
