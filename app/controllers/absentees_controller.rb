class AbsenteesController < ApplicationController
  before_action :set_absentee, only: [:show, :edit, :update, :destroy]

  # GET /absentees
  # GET /absentees.json
  def index
    @absentees = Absentee.all
  end

  # GET /absentees/1
  # GET /absentees/1.json
  def show
  end

  # GET /absentees/new
  def new
    @absentee = Absentee.new
  end

  # GET /absentees/1/edit
  def edit
  end

  # POST /absentees
  # POST /absentees.json
  def create
    @absentee = Absentee.new(absentee_params)

    respond_to do |format|
      if @absentee.save
        format.html { redirect_to @absentee, notice: 'Absentee was successfully created.' }
        format.json { render :show, status: :created, location: @absentee }
      else
        format.html { render :new }
        format.json { render json: @absentee.errors, status: :unprocessable_entity }
      end
    end
  end

  def absent_report
    start_date = Date.today.beginning_of_year # your start
    end_date = Date.today.end_of_year # your end
    my_days = [5,6,0] # day of the week in 0-6. Sunday is day-of-week 0; Saturday is day-of-week 6.
    @result = (start_date..end_date).to_a.select {|k| my_days.include?(k.wday)}
    @members = Member.all
    @absentees = Absentee.all
  end

  # PATCH/PUT /absentees/1
  # PATCH/PUT /absentees/1.json
  def update
    respond_to do |format|
      if @absentee.update(absentee_params)
        format.html { redirect_to @absentee, notice: 'Absentee was successfully updated.' }
        format.json { render :show, status: :ok, location: @absentee }
      else
        format.html { render :edit }
        format.json { render json: @absentee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /absentees/1
  # DELETE /absentees/1.json
  def destroy
    @absentee.destroy
    respond_to do |format|
      format.html { redirect_to absentees_url, notice: 'Absentee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_absentee
      @absentee = Absentee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def absentee_params
      params.require(:absentee).permit(:member_id, :function_date, :remarks)
    end
end
