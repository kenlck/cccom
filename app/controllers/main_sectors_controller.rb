class MainSectorsController < ApplicationController
  before_action :set_main_sector, only: [:show, :edit, :update, :destroy]

  # GET /main_sectors
  # GET /main_sectors.json
  def index
    @main_sectors = MainSector.all
  end

  # GET /main_sectors/1
  # GET /main_sectors/1.json
  def show
  end

  # GET /main_sectors/new
  def new
    @main_sector = MainSector.new
  end

  # GET /main_sectors/1/edit
  def edit
  end

  # POST /main_sectors
  # POST /main_sectors.json
  def create
    @main_sector = MainSector.new(main_sector_params)

    respond_to do |format|
      if @main_sector.save
        format.html { redirect_to @main_sector, notice: 'Main sector was successfully created.' }
        format.json { render :show, status: :created, location: @main_sector }
      else
        format.html { render :new }
        format.json { render json: @main_sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /main_sectors/1
  # PATCH/PUT /main_sectors/1.json
  def update
    respond_to do |format|
      if @main_sector.update(main_sector_params)
        format.html { redirect_to @main_sector, notice: 'Main sector was successfully updated.' }
        format.json { render :show, status: :ok, location: @main_sector }
      else
        format.html { render :edit }
        format.json { render json: @main_sector.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /main_sectors/1
  # DELETE /main_sectors/1.json
  def destroy
    @main_sector.destroy
    respond_to do |format|
      format.html { redirect_to main_sectors_url, notice: 'Main sector was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_main_sector
      @main_sector = MainSector.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def main_sector_params
      params.require(:main_sector).permit(:sector_name, :staff_id)
    end
end
