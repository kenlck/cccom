class LeaderTransactionsController < ApplicationController
  before_action :set_leader_transaction, only: [:show, :edit, :update, :destroy]

  # GET /leader_transactions
  # GET /leader_transactions.json
  def index
    @leader_transactions = LeaderTransaction.all
  end

  # GET /leader_transactions/1
  # GET /leader_transactions/1.json
  def show
  end

  # GET /leader_transactions/new
  def new
    @leader_transaction = LeaderTransaction.new
  end

  # GET /leader_transactions/1/edit
  def edit
  end

  # POST /leader_transactions
  # POST /leader_transactions.json
  def create
    @leader_transaction = LeaderTransaction.new(leader_transaction_params)

    respond_to do |format|
      if @leader_transaction.save
        format.html { redirect_to @leader_transaction, notice: 'Leader transaction was successfully created.' }
        format.json { render :show, status: :created, location: @leader_transaction }
      else
        format.html { render :new }
        format.json { render json: @leader_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leader_transactions/1
  # PATCH/PUT /leader_transactions/1.json
  def update
    respond_to do |format|
      if @leader_transaction.update(leader_transaction_params)
        format.html { redirect_to @leader_transaction, notice: 'Leader transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @leader_transaction }
      else
        format.html { render :edit }
        format.json { render json: @leader_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leader_transactions/1
  # DELETE /leader_transactions/1.json
  def destroy
    @leader_transaction.destroy
    respond_to do |format|
      format.html { redirect_to leader_transactions_url, notice: 'Leader transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leader_transaction
      @leader_transaction = LeaderTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leader_transaction_params
      params.fetch(:leader_transaction, {})
    end
end
