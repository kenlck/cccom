class PledgesController < ApplicationController
  include ApplicationHelper
  before_action :set_pledge, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /pledges
  # GET /pledges.json
  def index
    @pledges = Pledge.all.order(year: :desc)
    if params[:year] && !params[:year].empty?
      @selectedYear = Date.new(params[:year].to_i)
      @pledges = @pledges.query_by_year(@selectedYear)
    end
    @pledges = @pledges.order(year: :desc).where(member: Member.search(params[:name])) if params[:name] && !params[:name].empty?

    @pledges = @pledges.paginate(page: params[:page], per_page: 30)
  end

  def member
    @pledges = Pledge.all
    if params[:year] && !params[:year].empty?
      @selectedYear = Date.new(params[:year].to_i)
      @pledges = @pledges.query_by_year(@selectedYear)
    end
    if params[:condition] && params[:condition]=="no_pledge"
      members_id = Member.where(on_behalf: false).pluck(:id) - @pledges.pluck(:member_id)
    else
      members_id = Member.where(on_behalf: false).pluck(:id)
    end
    @members = Member.includes(:sector, :pledges).where(id: members_id)
    @members = @members.search(params[:name]) if params[:name] && !params[:name].empty?
    @sectors = Sector.where(active: true).pluck(:id)
    respond_to do |format|
      format.html{
        @members = @members.order(:sector_id, :gender).sort_by{ |x| x.sector.id }.paginate(page: params[:page], per_page: 30)
      }
      format.csv {
        @members = @members.order(:sector_id, :gender).select { |x| @sectors.include?(x.sector_id.to_i)}
        # change this -> create a new function generate_member_pledge_csv
        send_data generate_member_pledge_csv(@members)
      }
    end

  end

  # GET /pledges/1
  # GET /pledges/1.json
  def show
  end

  # GET /pledges/new
  def new
    @pledge = Pledge.new
    @pledge.member = Member.search(params[:name]).first if params[:name] && !params[:name].empty?
  end
  # GET /pledges/1/edit
  def edit
  end

  # POST /pledges
  # POST /pledges.json
  def create
    @pledge = Pledge.new(pledge_params)

    respond_to do |format|
      if @pledge.save
        format.html { redirect_to @pledge, notice: 'Pledge was successfully created.' }
        format.json { render :show, status: :created, location: @pledge }
      else
        format.html { render :new }
        format.json { render json: @pledge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pledges/1
  # PATCH/PUT /pledges/1.json
  def update
    respond_to do |format|
      if @pledge.update(pledge_params)
        format.html { redirect_to @pledge, notice: 'Pledge was successfully updated.' }
        format.json { render :show, status: :ok, location: @pledge }
      else
        format.html { render :edit }
        format.json { render json: @pledge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pledges/1
  # DELETE /pledges/1.json
  def destroy
    @pledge.destroy
    respond_to do |format|
      format.html { redirect_to pledges_url, notice: 'Pledge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pledge
      @pledge = Pledge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pledge_params
      params.require(:pledge).permit(:year, :amount, :member_id, :building_fund_pledge, :special_contribution_pledge, :amount_percentage)
    end
end
