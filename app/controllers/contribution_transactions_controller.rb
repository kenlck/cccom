class ContributionTransactionsController < ApplicationController
  include ApplicationHelper
  before_action :set_contribution_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, except: [:member_records, :summary, :summary2, :summary3]
  before_action :authenticate_user!, only: [:member_records, :summary]

  # GET /contribution_transactions
  # GET /contribution_transactions.json
  def index
    @contribution_transaction = ContributionTransaction.all.order(bank_statement_date: :desc)
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(member: Member.search(params[:name])) if params[:name] && !params[:name].empty?
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(paid_by: Member.search(params[:paid_by])) if params[:paid_by] && !params[:paid_by].empty?

    if params[:fund_type] && !params[:fund_type].empty? && params[:fund_type] == "special_contribution"
      @contribution_transaction = @contribution_transaction.query_by_month(params[:year]) if params[:year] && !params[:year].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_year(params[:year]) if params[:year] && !params[:year].empty?
    else
      @contribution_transaction = @contribution_transaction.query_by_year(params[:month]) if params[:month] && !params[:month].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_month(params[:month]) if params[:month] && !params[:month].empty?
    end

    if params[:fund_type] && !params[:fund_type].empty? && params[:fund_type] == "special_contribution"
      @contribution_transaction = @contribution_transaction.query_by_month2(params[:bank_statement]) if params[:bank_statement] && !params[:bank_statement].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_year2(params[:bank_statement]) if params[:bank_statement] && !params[:bank_statement].empty?
    else
      @contribution_transaction = @contribution_transaction.query_by_year2(params[:bank_statement]) if params[:bank_statement] && !params[:bank_statement].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_month2(params[:bank_statement]) if params[:bank_statement] && !params[:bank_statement].empty?
    end


    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_sector(params[:sector]) if params[:sector] && !params[:sector].empty?
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(bank_account: params[:bank]) if params[:bank] && !params[:bank].empty?
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(fund_type: params[:fund_type]) if params[:fund_type] && !params[:fund_type].empty?
    if params[:lock]
      @contribution_transaction.update_all(editable: false)
    end
    if params[:unlock]
      @contribution_transaction.update_all(editable: true)
    end
    respond_to do |format|
      format.html{
        @contribution_transactions = @contribution_transaction.paginate(page: params[:page], per_page: 30)
      }
      format.csv {
        send_data generate_contribution_transactions_csv(@contribution_transaction)
      }
    end
  end

  def tpri
    @contribution_transaction = ContributionTransaction.where(bank_account: "TPRI")
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(member: Member.search(params[:name])) if params[:name] && !params[:name].empty?
    if params[:fund_type] && !params[:fund_type].empty? && params[:fund_type] == "special_contribution"
      @contribution_transaction = @contribution_transaction.query_by_month2(params[:year]) if params[:year] && !params[:year].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_year(params[:year]) if params[:year] && !params[:year].empty?
    else
      @contribution_transaction = @contribution_transaction.query_by_year2(params[:month]) if params[:month] && !params[:month].empty?
      @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).query_by_month2(params[:month]) if params[:month] && !params[:month].empty?
    end
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(bank_account: params[:bank]) if params[:bank] && !params[:bank].empty?
    @contribution_transaction = @contribution_transaction.order(bank_statement_date: :desc).where(fund_type: params[:fund_type]) if params[:fund_type] && !params[:fund_type].empty?

    respond_to do |format|
      format.html{
        @contribution_transactions = @contribution_transaction.paginate(page: params[:page], per_page: 30)
      }
      format.csv {
        send_data generate_tpri_csv(@contribution_transaction)
      }
    end

  end

  def member
    @sectors = Sector.where(active: true).pluck(:id)
    @contribution_transaction = ContributionTransaction.all
    @contribution_transaction = @contribution_transaction.where(fund_type: params[:fund_type]) if params[:fund_type] && !params[:fund_type].empty?
    if params[:fund_type] && !params[:fund_type].empty? && params[:fund_type] == "special_contribution"
      @contribution_transaction = @contribution_transaction.query_by_year(params[:month]) if params[:month] && !params[:month].empty?
    else
      @contribution_transaction = @contribution_transaction.query_by_month(params[:month]) if params[:month] && !params[:month].empty?
    end

    @contribution_transactions = @contribution_transaction.where(late_pay: true) if params[:condition] && params[:condition]=="late_pay"
    @contribution_transactions = @contribution_transaction if params[:condition] && params[:condition]=="no_pay"
    # byebug
    if params[:condition] && params[:condition]=="late_pay"
      members_id = @contribution_transactions.pluck(:member_id)
    elsif params[:condition] && params[:condition]=="no_pay"
      members_id = Member.where(on_behalf: false).pluck(:id) - @contribution_transactions.pluck(:member_id)
    else
      members_id = Member.where(on_behalf: false).pluck(:id)
    end
    @members = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id)
    @members = @members.search(params[:name]) if params[:name] && !params[:name].empty?
    if params[:reminder] == 'true' && params[:condition] == 'no_pay' && params[:month] && !params[:month].empty?
      @members.each do |member|
        if member.email && !member.email.empty?
          month = params[:month]
          UserNotifierMailer.send_reminder(member, month).deliver_later!
        end
      end
    end
    respond_to do |format|
      format.html{
        @members = @members.order(:sector_id, :gender).sort_by{ |x| x.sector.id }.paginate(page: params[:page], per_page: 30)
      }
      format.csv {
        @members = @members.order(:sector_id, :gender).select { |x| @sectors.include?(x.sector_id.to_i)}
        send_data generate_nongivers_csv(@members)
      }
    end
  end

  def summary
    if params[:condition] && params[:condition]=="late_pay"
      members_id = @contribution_transactions.pluck(:member_id)
    elsif params[:condition] && params[:condition]=="no_pay"
      members_id = Member.pluck(:id) - @contribution_transactions.pluck(:member_id)
    else
      members_id = Member.pluck(:id)
    end
    @member = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15,17,18], on_behalf: false)
    @memberTotal = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15,17,18])
    @membersTotal = @memberTotal.group_by(&:sector_id)

    @members = @member.group_by(&:sector_id)

    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @pledges = []
    @monthContributionNotGiven = []
    @buildingFundNotGiven = []
    @members.keys.each do |key|
      total = 0
      totalPledgesNotGiven = 0
      totalBuildingFundNotGiven = 0
      @members[key].each do |member|
        range = (("1/1/"+year).to_date.beginning_of_year..month.end_of_month)
        my_pledge = member.pledges.select{ |x| range.cover?x.year }.pluck(:amount).sum
        if member.pledges.select{ |x| range.cover?x.year }.length == 0
          my_pledge = member.pledges.last.amount if member.pledges.last
          my_pledge = 0 unless member.pledges.last
        end
        my_building_fund = member.pledges.select{ |x| range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| range.cover?x.year }.length == 0
          my_building_fund = member.pledges.last.building_fund_pledge if member.pledges.last
          my_building_fund = 0 unless member.pledges.last
        end

        total = total + my_pledge
        myTransactions = member.contribution_transactions.select{ |x| x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount)
        if myTransactions.length < month.month
          outstandingMonths = (month.month - myTransactions.length) * my_pledge
          totalPledgesNotGiven += outstandingMonths
        end

        myBuildingFunds = member.contribution_transactions.select{ |x| x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:building_fund_pledge)
        if myBuildingFunds.length < month.month
          outstandingBuilding = (month.month - myBuildingFunds.length) * my_building_fund
          totalBuildingFundNotGiven += outstandingBuilding
        end
      end
      @pledges.append(total)
      @monthContributionNotGiven.append(totalPledgesNotGiven)
      @buildingFundNotGiven.append(totalBuildingFundNotGiven)
    end

    @buildingpledges = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (("1/1/"+year).to_date.beginning_of_year..month.end_of_month)
        my_building_fund = member.pledges.select{ |x| range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| range.cover?x.year }.length == 0
          my_building_fund = member.pledges.last.building_fund_pledge if member.pledges.last
          my_building_fund = 0 unless member.pledges.last
        end
        total = total + my_building_fund
      end
      @buildingpledges.append(total)
    end

    @specialpledges = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
        my_special_fund = member.pledges.select{ |x| range.cover?x.year }.pluck(:special_contribution_pledge).sum
        if member.pledges.select{ |x| range.cover?x.year }.length == 0
          my_special_fund = member.pledges.last.special_contribution_pledge if member.pledges.last
          my_special_fund = 0 unless member.pledges.last
        end
        total = total + my_special_fund
      end
      @specialpledges.append(total)
    end

    @tpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @tpri.append(total)
    end

    @specialtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialtpri.append(total)
    end

    @buildingtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingtpri.append(total)
    end

    @cccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @cccom.append(total)
    end

    @specialcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialcccom.append(total)
    end

    @buildingcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingcccom.append(total)
    end
  end

  # This is for the second summary page, where all the graphs are displayed
  def summary2
    if params[:condition] && params[:condition]=="late_pay"
      members_id = @contribution_transactions.pluck(:member_id)
    elsif params[:condition] && params[:condition]=="no_pay"
      members_id = Member.pluck(:id) - @contribution_transactions.pluck(:member_id)
    else
      members_id = Member.pluck(:id)
    end
    @member = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15])

    @members = @member.group_by(&:sector_id)

    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @pledges = []
    @monthContributionNotGiven = []
    @buildingFundNotGiven = []
    @members.keys.each do |key|
      total = 0
      totalPledgesNotGiven = 0
      totalBuildingFundNotGiven = 0
      @members[key].each do |member|
        range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
        my_pledge = member.pledges.select{ |x| range.cover?x.year }.pluck(:amount).sum
        if member.pledges.select{ |x| range.cover?x.year }.length == 0
          my_pledge = member.pledges.last.amount if member.pledges.last
          my_pledge = 0 unless member.pledges.last
        end
        total = total + my_pledge
      end
      @pledges.append(total)
    end


    # for graph
    @graph_pledges_array = []
    @graph_pledges_hash = Pledge.all.group_by{ |x| x.year.year }
    @graph_pledges_hash.keys.each { |year123| @graph_pledges_array.append(@graph_pledges_hash[year123].pluck(:amount).sum()) }

    @graph_special_contribution_array = []
    @graph_special_contribution_hash = ContributionTransaction.includes(member: :sector).where(fund_type: 'special_contribution').group_by{ |x| x.transaction_date.year }
    @graph_special_contribution_hash.keys.each { |year123| @graph_special_contribution_array.append(@graph_special_contribution_hash[year123].pluck(:amount).sum()) }


    @graph_contribution_transaction_month_hash = ContributionTransaction.includes(member: :sector).where(fund_type: 'contribution').query_by_year('1/1/'+year).group_by{ |x| [x.transaction_date.month, x.member.sector.sector_type] }

    @graph_contribution_transaction_month2_hash = {}
    @graph_contribution_transaction_month_hash.keys.each { |key| @graph_contribution_transaction_month2_hash[key] = (@graph_contribution_transaction_month_hash[key].pluck(:amount).sum()) }

    @graph_building_fund_month_hash = ContributionTransaction.includes(member: :sector).where(fund_type: 'building_fund').query_by_year('1/1/'+year).group_by{ |x| [x.transaction_date.month, x.member.sector.sector_type] }

    @graph_building_fund_month2_hash = {}
    @graph_building_fund_month_hash.keys.each { |key| @graph_building_fund_month2_hash[key] = (@graph_building_fund_month_hash[key].pluck(:amount).sum()) }

    range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
    @ct_over_year = ContributionTransaction.includes(member: :sector).where(fund_type: 'contribution').select{ |x| range.cover?x.transaction_date }.group_by { |m| m.member.sector_id }
    @year = year
  end

  def summary3
    if params[:condition] && params[:condition]=="late_pay"
      members_id = @contribution_transactions.pluck(:member_id)
    elsif params[:condition] && params[:condition]=="no_pay"
      members_id = Member.pluck(:id) - @contribution_transactions.pluck(:member_id)
    else
      members_id = Member.pluck(:id)
    end
    @member = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15], on_behalf: false)

    @members = @member.order('sectors.sector_type ASC').group_by(&:sector_id)

    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @pledges = []
    @monthContributionNotGiven = []
    @buildingFundNotGiven = []
    @buildingFundNotGivenCount = []
    @monthContributionNotGivenCount = []
    year_range = (month.to_date.beginning_of_year..month.to_date.end_of_year)
    range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
    @members.keys.each do |key|
      total = 0
      totalPledgesNotGiven = 0
      totalBuildingFundNotGiven = 0
      totalBuildingFundNotGivenCount = 0
      monthContributionNotGivenCount = 0
      @members[key].each do |member|
        my_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:amount).sum
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_pledge = member.pledges.last.amount if member.pledges.last
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_pledge = 0 unless member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        total = total + my_pledge
        myTransactions = member.contribution_transactions.select{ |x| x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount)
        if myTransactions.length < 1
          outstandingMonths = my_pledge
          totalPledgesNotGiven += outstandingMonths
          monthContributionNotGivenCount += 1
        end
        myBuildingFunds = member.contribution_transactions.select{ |x| x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:building_fund_pledge)
        if myBuildingFunds.length < 1
          outstandingBuilding = my_building_pledge
          totalBuildingFundNotGiven += outstandingBuilding
          totalBuildingFundNotGivenCount += 1
        end
      end
      @pledges.append(total)
      @monthContributionNotGiven.append(totalPledgesNotGiven)
      @buildingFundNotGiven.append(totalBuildingFundNotGiven)
      @monthContributionNotGivenCount.append(monthContributionNotGivenCount)
      @buildingFundNotGivenCount.append(totalBuildingFundNotGivenCount)
    end

    @buildingpledges = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
        total = total + my_building_pledge
      end
      @buildingpledges.append(total)
    end

    @specialpledges = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        my_special_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:special_contribution_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_special_pledge = member.pledges.last.special_contribution_pledge if member.pledges.last
          my_special_pledge = 0 unless member.pledges.last
        end
        total = total + my_special_pledge
      end
      @specialpledges.append(total)
    end

    @tpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @tpri.append(total)
    end

    @specialtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialtpri.append(total)
    end

    @buildingtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingtpri.append(total)
    end

    @cccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @cccom.append(total)
    end

    @specialcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialcccom.append(total)
    end

    @buildingcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingcccom.append(total)
    end


    #men
    @memberMen = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15], on_behalf: false, gender: true)

    @membersMen = @memberMen.order('sectors.sector_type ASC').group_by(&:sector_id)

    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @pledgesMen = []
    @monthContributionNotGivenMen = []
    @buildingFundNotGivenMen = []
    @buildingFundNotGivenCountMen = []
    @monthContributionNotGivenCountMen = []
    year_range = (month.to_date.beginning_of_year..month.to_date.end_of_year)
    range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
    @membersMen.keys.each do |key|
      total = 0
      totalPledgesNotGiven = 0
      totalBuildingFundNotGiven = 0
      my_pledge = 0
      my_building_pledge = 0
      monthContributionNotGivenCountMen = 0
      totalBuildingFundNotGivenCountMen = 0
      @membersMen[key].each do |member|
        my_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:amount).sum
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_pledge = member.pledges.last.amount if member.pledges.last
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_pledge = 0 unless member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        total = total + my_pledge
        myTransactions = member.contribution_transactions.select{ |x| x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount)
        if myTransactions.length < 1
          outstandingMonths = my_pledge
          totalPledgesNotGiven += outstandingMonths
          monthContributionNotGivenCountMen += 1
        end
        myBuildingFunds = member.contribution_transactions.select{ |x| x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:building_fund_pledge)
        if myBuildingFunds.length < 1
          outstandingBuilding = my_building_pledge
          totalBuildingFundNotGiven += outstandingBuilding
          totalBuildingFundNotGivenCountMen += 1
        end
      end
      @pledgesMen.append(total)
      @monthContributionNotGivenMen.append(totalPledgesNotGiven)
      @buildingFundNotGivenMen.append(totalBuildingFundNotGiven)
      @monthContributionNotGivenCountMen.append(monthContributionNotGivenCountMen)
      @buildingFundNotGivenCountMen.append(totalBuildingFundNotGivenCountMen)
    end

    @buildingpledgesMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
        total = total + my_building_pledge
      end
      @buildingpledgesMen.append(total)
    end

    @specialpledgesMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        my_special_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:special_contribution_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_special_pledge = member.pledges.last.special_contribution_pledge if member.pledges.last
          my_special_pledge = 0 unless member.pledges.last
        end
        total = total + my_special_pledge
      end
      @specialpledgesMen.append(total)
    end

    @tpriMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @tpriMen.append(total)
    end

    @specialtpriMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialtpriMen.append(total)
    end

    @buildingtpriMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingtpriMen.append(total)
    end

    @cccomMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @cccomMen.append(total)
    end

    @specialcccomMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialcccomMen.append(total)
    end

    @buildingcccomMen = []
    @membersMen.keys.each do |key|
      total = 0
      @membersMen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingcccomMen.append(total)
    end

    #women
    @memberWomen = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,15], on_behalf: false, gender: false)

    @membersWomen = @memberWomen.order('sectors.sector_type ASC').group_by(&:sector_id)

    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @pledgesWomen = []
    @monthContributionNotGivenWomen = []
    @buildingFundNotGivenWomen = []
    @buildingFundNotGivenCountWomen = []
    @monthContributionNotGivenCountWomen = []
    year_range = (month.to_date.beginning_of_year..month.to_date.end_of_year)
    range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
    @membersWomen.keys.each do |key|
      total = 0
      totalPledgesNotGiven = 0
      totalBuildingFundNotGiven = 0
      monthContributionNotGivenCountWomen = 0
      totalBuildingFundNotGivenCountWomen = 0
      @membersWomen[key].each do |member|
        my_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:amount).sum
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_pledge = member.pledges.last.amount if member.pledges.last
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_pledge = 0 unless member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        total = total + my_pledge
        myTransactions = member.contribution_transactions.select{ |x| x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount)
        if myTransactions.length < 1
          outstandingMonths = my_pledge
          totalPledgesNotGiven += outstandingMonths
          monthContributionNotGivenCountWomen += 1
        end
        myBuildingFunds = member.contribution_transactions.select{ |x| x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:building_fund_pledge)
        if myBuildingFunds.length < 1
          outstandingBuilding = my_building_pledge
          totalBuildingFundNotGiven += outstandingBuilding
          totalBuildingFundNotGivenCountWomen += 1
        end
      end
      @pledgesWomen.append(total)
      @monthContributionNotGivenWomen.append(totalPledgesNotGiven)
      @buildingFundNotGivenWomen.append(totalBuildingFundNotGiven)
      @monthContributionNotGivenCountWomen.append(monthContributionNotGivenCountWomen)
      @buildingFundNotGivenCountWomen.append(totalBuildingFundNotGivenCountWomen)

    end

    @buildingpledgesWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        my_building_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:building_fund_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_building_pledge = member.pledges.last.building_fund_pledge if member.pledges.last
          my_building_pledge = 0 unless member.pledges.last
        end
        range = (("1/1/"+year).to_date.beginning_of_year..("1/1/"+year).to_date.end_of_year)
        total = total + my_building_pledge
      end
      @buildingpledgesWomen.append(total)
    end

    @specialpledgesWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        my_special_pledge = member.pledges.select{ |x| year_range.cover?x.year }.pluck(:special_contribution_pledge).sum
        if member.pledges.select{ |x| year_range.cover?x.year }.length == 0
          my_special_pledge = member.pledges.last.special_contribution_pledge if member.pledges.last
          my_special_pledge = 0 unless member.pledges.last
        end
        total = total + my_special_pledge
      end
      @specialpledgesWomen.append(total)
    end

    @tpriWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @tpriWomen.append(total)
    end

    @specialtpriWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialtpriWomen.append(total)
    end

    @buildingtpriWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingtpriWomen.append(total)
    end

    @cccomWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @cccomWomen.append(total)
    end

    @specialcccomWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_year..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @specialcccomWomen.append(total)
    end

    @buildingcccomWomen = []
    @membersWomen.keys.each do |key|
      total = 0
      @membersWomen[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.transaction_date}.pluck(:amount).sum
      end
      @buildingcccomWomen.append(total)
    end
  end

  def summary4

    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @sectors = Sector.where(active: true, payable: true)
    @main_sectors = MainSector.all

    @cccomAmountTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)
    @tpriAmountTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)
    @cccomAmountMale = Member.includes(:sector, :contribution_transactions).where(gender: true, 'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)
    @tpriAmountMale = Member.includes(:sector, :contribution_transactions).where(gender: true,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)
    @cccomAmountFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)
    @tpriAmountFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'contribution').group(:sector_id).sum(:amount)

    @cccomBuildingTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)
    @tpriBuildingTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)
    @cccomBuildingMale = Member.includes(:sector, :contribution_transactions).where(gender: true,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)
    @tpriBuildingMale = Member.includes(:sector, :contribution_transactions).where(gender: true,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)
    @cccomBuildingFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)
    @tpriBuildingFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.transaction_date': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'building_fund').group(:sector_id).sum(:amount)

    @cccomSpecialTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)
    @tpriSpecialTotal = Member.includes(:sector, :contribution_transactions).where('contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)
    @cccomSpecialMale = Member.includes(:sector, :contribution_transactions).where(gender: true,'contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)
    @tpriSpecialMale = Member.includes(:sector, :contribution_transactions).where(gender: true,'contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)
    @cccomSpecialFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'CCCOM', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)
    @tpriSpecialFemale = Member.includes(:sector, :contribution_transactions).where(gender: false,'contribution_transactions.created_at': [month.to_date.beginning_of_month...month.to_date.end_of_month], 'contribution_transactions.bank_account': 'TPRI', 'contribution_transactions.fund_type': 'special_contribution').group(:sector_id).sum(:amount)

    @pledges = Pledge.includes(:member).unscoped.where('year < ?', month.end_of_year).select('DISTINCT ON (pledges.member_id) *').order('pledges.member_id, pledges.year DESC').group_by{|p| p.member.sector_id}

    @givers_id_ct = ContributionTransaction.unscoped.where(transaction_date: [month.beginning_of_month...month.end_of_month], fund_type: 'contribution').select('DISTINCT ON (member_id) *').pluck(:member_id)
    @givers_id_bf = ContributionTransaction.unscoped.where(transaction_date: [month.beginning_of_month...month.end_of_month], fund_type: 'building_fund').select('DISTINCT ON (member_id) *').pluck(:member_id)
    @givers_id_sc = ContributionTransaction.unscoped.where(transaction_date: [month.beginning_of_month...month.end_of_month], fund_type: 'special_contribution').select('DISTINCT ON (member_id) *').pluck(:member_id)

    @non_givers_ct = Member.all.where.not(id: @givers_id_ct).group_by{|p| p.sector_id}
    @non_givers_bf = Member.all.where.not(id: @givers_id_bf).group_by{|p| p.sector_id}
    @non_givers_sc = Member.all.where.not(id: @givers_id_sc).group_by{|p| p.sector_id}
    @pledges_not_given_bf = Pledge.includes(:member).unscoped.where.not(member_id: @givers_id_bf).where('year < ?', month.end_of_year).select('DISTINCT ON (pledges.member_id) *').order('pledges.member_id, pledges.year DESC').group_by{|p| p.member.sector_id}
    @pledges_not_given_ct = Pledge.includes(:member).unscoped.where.not(member_id: @givers_id_ct).where('year < ?', month.end_of_year).select('DISTINCT ON (pledges.member_id) *').order('pledges.member_id, pledges.year DESC').group_by{|p| p.member.sector_id}
    @pledges_not_given_sc = Pledge.includes(:member).unscoped.where.not(member_id: @givers_id_sc).where('year < ?', month.end_of_year).select('DISTINCT ON (pledges.member_id) *').order('pledges.member_id, pledges.year DESC').group_by{|p| p.member.sector_id}
    # @nongivers =
  end

  def bank_summary
    members_id = Member.pluck(:id)

    @member = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [1,2,4,5,6,7,8,9,10, 14, 15,17,18])
    @members = @member.group_by(&:sector_id)

    @member = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector_id: [16])
    @noslips = @member.group_by(&:sector_id)


    year = (params[:month] && !params[:month].empty?) ? params[:month].to_date.year.to_s : Date.today.year.to_s
    month = (params[:month] && !params[:month].empty?) ? params[:month].to_date : Date.today.to_date

    @tpri = []
    @tpri_transaction = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
        @tpri_transaction.concat(member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"})
      end
      @tpri.append(total)
    end

    @tprinoslip = []
    @tpri_transaction_noslip = []
    @noslips.keys.each do |key|
      total = 0
      @noslips[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
        @tpri_transaction_noslip.concat(member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "contribution"})
      end
      @tprinoslip.append(total)
    end

    @specialtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @specialtpri.append(total)
    end

    @buildingtpri = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "TPRI" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @buildingtpri.append(total)
    end

    @cccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @cccom.append(total)
    end

    @cccomnoslip = []
    @noslips.keys.each do |key|
      total = 0
      @noslips[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @cccomnoslip.append(total)
    end

    @specialcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "special_contribution"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @specialcccom.append(total)
    end

    @buildingcccom = []
    @members.keys.each do |key|
      total = 0
      @members[key].each do |member|
        range = (month.to_date.beginning_of_month..month.to_date.end_of_month)
        total = total + member.contribution_transactions.select{ |x| x.bank_account == "CCCOM" && x.fund_type == "building_fund"}.select{ |x| range.cover?x.bank_statement_date}.pluck(:amount).sum
      end
      @buildingcccom.append(total)
    end

    respond_to do |format|
      format.csv {
        send_data generate_tpri_csv(@tpri_transaction)
      }
      format.html {}
    end
  end

  # GET /contribution_transactions/1
  # GET /contribution_transactions/1.json
  def show
  end

  # GET /contribution_transactions/new
  def new
    @contribution_transaction = ContributionTransaction.new
    @contribution_transaction.member = Member.search(params[:name]).first if params[:name] && !params[:name].empty?
    @contribution_transaction.receipt_to = Member.search(params[:name]).first if params[:name] && !params[:name].empty?
    @contribution_transaction.paid_by = Member.search(params[:name]).first if params[:name] && !params[:name].empty?
    @contribution_transaction.bank_account = params[:bank] if params[:bank] && !params[:bank].empty?
    @contribution_transaction.transaction_date = params[:month].to_date if params[:month] && !params[:month].empty?
    @contribution_transaction.fund_type = params[:fund_type] if params[:fund_type] && !params[:fund_type].empty?
  end

  # GET /contribution_transactions/1/edit
  def edit
  end

  # POST /contribution_transactions
  # POST /contribution_transactions.json
  def create
    members = contribution_transaction_params[:member_ids].split(',')
    amounts = contribution_transaction_params[:amount].split('-')

    if(members.length > 0)
      # members.each_with_index do |m, index|
      #   m_contrib_params = contribution_transaction_params
      #   m_contrib_params[:member_id] = m
      #   m_contrib_params[:amount] = amounts[index].to_f
      #   @contribution_transaction = ContributionTransaction.create(m_contrib_params) if amounts[index].to_f > 0
      # end
      m_contrib_params = contribution_transaction_params
      # m_contrib_params[:member_id] = members.first
      @contribution_transaction = ContributionTransaction.create(m_contrib_params)
    else
      @contribution_transaction = ContributionTransaction.create(contribution_transaction_params)
    end

    respond_to do |format|
      if @contribution_transaction
        format.html { redirect_to @contribution_transaction, notice: 'Contribution transaction was successfully created.' }
        format.json { render :show, status: :created, location: @contribution_transaction }
      else
        format.html { render :new }
        format.json { render json: @contribution_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contribution_transactions/1
  # PATCH/PUT /contribution_transactions/1.json
  def update
    respond_to do |format|
      if @contribution_transaction.editable && @contribution_transaction.update(contribution_transaction_params)
        format.html {
          if request.referer.split('/')[-1] == 'contribution_transactions'
            redirect_to contribution_transactions_path, notice: 'Contribution transaction was successfully updated.'
          else
            redirect_to @contribution_transaction, notice: 'Contribution transaction was successfully updated.'
          end
        }
        format.json { render :show, status: :ok, location: @contribution_transaction }
      else
        if @contribution_transaction.editable
          format.html { render :edit }
          format.json { render json: @contribution_transaction.errors, status: :unprocessable_entity }
        else
          flash.now[:notice] = "Data locked"
          format.html { render :edit }
          format.json { render json: 'Data locked', status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /contribution_transactions/1
  # DELETE /contribution_transactions/1.json
  def destroy
    if @contribution_transaction.editable
      @contribution_transaction.destroy
      respond_to do |format|
        format.html { redirect_to contribution_transactions_url, notice: 'Contribution transaction was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to contribution_transactions_url, notice: 'Contribution transaction was failedfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  def member_records
    @sectors = Sector.where(active: true).pluck(:id)
    @contribution_transaction = ContributionTransaction.all

    @contribution_transaction = @contribution_transaction.where(fund_type: params[:fund_type]) if params[:fund_type] && !params[:fund_type].empty?
    if params[:fund_type] && !params[:fund_type].empty? && params[:fund_type] == "special_contribution"
      @contribution_transaction = @contribution_transaction.query_by_year(params[:month]) if params[:month] && !params[:month].empty?
    else
      @contribution_transaction = @contribution_transaction.query_by_month(params[:month]) if params[:month] && !params[:month].empty?
    end
    @contribution_transactions = @contribution_transaction.where(late_pay: true) if params[:condition] && params[:condition]=="late_pay"
    @contribution_transactions = @contribution_transaction if params[:condition] && params[:condition]=="no_pay"
    if params[:condition] && params[:condition]=="late_pay"
      members_id = @contribution_transactions.pluck(:member_id)
    elsif params[:condition] && params[:condition]=="no_pay"
      members_id = Member.where(on_behalf: false).pluck(:id) - @contribution_transactions.pluck(:member_id)
    else
      members_id = Member.where(on_behalf: false).pluck(:id)
    end
    #.boss
    @my_sector = current_user.boss ? Sector.all.pluck(:id) : current_user.sectors.pluck(:id)
    @members = Member.includes(:contribution_transactions, :sector, :pledges).where(id: members_id, sector: @my_sector)
    @members = @members.search(params[:name]) if params[:name] && !params[:name].empty?

    respond_to do |format|
      format.html{
        @members = @members.order(:sector_id, :gender).sort_by{ |x| x.sector.id }.paginate(page: params[:page], per_page: 30)
      }
      format.csv {
        @members = @members.order(:sector_id, :gender).select { |x| @sectors.include?(x.sector_id.to_i)}
        send_data generate_member_csv(@members)
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution_transaction
      @contribution_transaction = ContributionTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contribution_transaction_params
      params.require(:contribution_transaction).permit(:transaction_date, :bank_statement_date,
          :amount, :member_id, :bank_account, :fund_type, :paid_by_id, :receipt_to_id, :comment, :member_ids, :created_at)
    end

    def authenticate_admin!
      if user_signed_in? && current_user.admin
        true
      else
        false
        redirect_to root_path, notice: 'Hello World'
      end
    end

    def hash_diff(first, second)
      first.
        dup.
        delete_if { |k, v| second[k] == v }.
        merge!(second.dup.delete_if { |k, v| first.has_key?(k) })
    end
end
