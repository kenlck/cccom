class SectorTransactionsController < ApplicationController
  before_action :set_sector_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  include ApplicationHelper

  # GET /sector_transactions
  # GET /sector_transactions.json
  def index
    if params[:year] && !params[:year].empty? && params[:start_month] && !params[:start_month].empty? && params[:end_month] && !params[:end_month].empty?
      t = Time.new(params[:year], params[:start_month]).in_time_zone('Singapore')
      t2 = Time.new(params[:year], params[:end_month]).in_time_zone('Singapore')
      @sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_month..t2.end_of_month).order(created_at: :desc, previous_sector_id: :desc).paginate(page: params[:page], per_page: 20)
      @my_sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_month..t2.end_of_month).order(created_at: :desc, previous_sector_id: :desc)
    elsif params[:year] && !params[:year].empty? && params[:start_month] && !params[:start_month].empty?
      t = Time.new(params[:year], params[:start_month]).in_time_zone('Singapore')
      @sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_month..t.end_of_month).order(created_at: :desc, previous_sector_id: :desc).paginate(page: params[:page], per_page: 20)
      @my_sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_month..t.end_of_month).order(created_at: :desc, previous_sector_id: :desc)
    elsif params[:year] && !params[:year].empty?
      t = Time.new(params[:year]).in_time_zone('Singapore')
      @sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_year..t.end_of_year).order(created_at: :desc, previous_sector_id: :desc).paginate(page: params[:page], per_page: 20)
      @my_sector_transactions = SectorTransaction.joins(:member).where(created_at: t.beginning_of_year..t.end_of_year).order(created_at: :desc, previous_sector_id: :desc)
    else
      @sector_transactions = SectorTransaction.joins(:member).order(created_at: :desc, previous_sector_id: :desc).paginate(page: params[:page], per_page: 20)
      @my_sector_transactions = SectorTransaction.joins(:member).order(created_at: :desc, previous_sector_id: :desc)
      @members = Member.all
    end

    respond_to do |format|
      format.html{
      }
      format.csv {
        if params[:year] && !params[:year].empty? && params[:start_month] && !params[:start_month].empty? && params[:end_month] && !params[:end_month].empty?
          t = Time.new(params[:year], params[:start_month]).in_time_zone('Singapore')
          t2 = Time.new(params[:year], params[:end_month]).in_time_zone('Singapore')
          @sector_transactions = SectorTransaction.all.where(created_at: t.beginning_of_month..t2.end_of_month).order(created_at: :desc, previous_sector_id: :desc)
        elsif params[:year] && !params[:year].empty? && params[:start_month] && !params[:start_month].empty?
          t = Time.new(params[:year], params[:start_month]).in_time_zone('Singapore')
          @sector_transactions = SectorTransaction.all.where(created_at: t.beginning_of_month..t.end_of_month).order(created_at: :desc, previous_sector_id: :desc)
        elsif params[:year] && !params[:year].empty?
          t = Time.new(params[:year]).in_time_zone('Singapore')
          @sector_transactions = SectorTransaction.all.where(created_at: t.beginning_of_year..t.end_of_year).order(created_at: :desc, previous_sector_id: :desc)
        else
          @sector_transactions = SectorTransaction.all.order(created_at: :desc, previous_sector_id: :desc)
          @members = Member.all
        end
        send_data generate_sector_report(@sector_transactions)
      }
      format.xls
    end

  end

  # GET /sector_transactions/1
  # GET /sector_transactions/1.json
  def show
  end

  # GET /sector_transactions/new
  def new
    @sector_transaction = SectorTransaction.new
  end

  # GET /sector_transactions/1/edit
  def edit
  end

  # POST /sector_transactions
  # POST /sector_transactions.json
  def create
    @sector_transaction = SectorTransaction.new(sector_transaction_params)

    respond_to do |format|
      if @sector_transaction.save
        format.html { redirect_to @sector_transaction, notice: 'Sector transaction was successfully created.' }
        format.json { render :show, status: :created, location: @sector_transaction }
      else
        format.html { render :new }
        format.json { render json: @sector_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sector_transactions/1
  # PATCH/PUT /sector_transactions/1.json
  def update
    respond_to do |format|
      if @sector_transaction.update(sector_transaction_params)
        format.html { redirect_to sector_transactions_path, notice: 'Sector transaction was successfully updated.' }
        format.json { render :index, status: :ok, location: @sector_transactions }
      else
        format.html { render :edit }
        format.json { render json: @sector_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sector_transactions/1
  # DELETE /sector_transactions/1.json
  def destroy
    @sector_transaction.destroy
    respond_to do |format|
      format.html { redirect_to sector_transactions_url, notice: 'Sector transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sector_transaction
      @sector_transaction = SectorTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sector_transaction_params
      params.require(:sector_transaction).permit(:created_at)
    end
end
