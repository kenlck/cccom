class MembersController < ApplicationController
  include ApplicationHelper
  before_action :set_member, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /members
  # GET /members.json
  def index
    @members = Member.all.includes(:sector, :leader)
    @members = @members.search_by_leader(params[:leader]) if params[:leader] && !params[:leader].empty?
    @members = @members.search(params[:search]) if params[:search] && !params[:search].empty?
    @members = @members.search_by_sector(params[:sector]) if params[:sector] && !params[:sector].empty?
    @members = @members.where(gender: params[:gender]) if params[:gender] && !params[:gender].empty?
    @members = @members.where(on_behalf: params[:on_behalf]) if params[:on_behalf] && !params[:on_behalf].empty?
    @members = @members.where(esi: params[:esi]) if params[:esi] && !params[:esi].empty?
    @members = @members.where(tpri: params[:tpri]) if params[:tpri] && !params[:tpri].empty?
    @members = @members.order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)


    respond_to do |format|
      format.html{
        @members = @members.paginate(page: params[:page], per_page: 50)
      }
      format.csv {
        @members = @members.order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        send_data generate_member_csv(@members)
      }
      format.xls
    end

  end

  def indexall

    if params[:search] && params[:sector] && params[:leader] && !params[:sector].empty? && !params[:leader].empty?
      @members = Member.paginate(page: params[:page], per_page: 50).search_by_leader(params[:leader]).search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
    elsif params[:search] && params[:sector] && !params[:sector].empty?
      @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
    elsif params[:search] && params[:leader] && !params[:leader].empty?
      @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).search_by_leader(params[:leader]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
    elsif params[:search]
      @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
    else
      @members = Member.paginate(page: params[:page], per_page: 50).all.includes(:sector, :leader).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
    end

    respond_to do |format|
      format.html{
        if params[:search] && params[:sector] && params[:leader] && !params[:sector].empty? && !params[:leader].empty?
          @members = Member.paginate(page: params[:page], per_page: 50).search_by_leader(params[:leader]).search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search] && params[:sector] && !params[:sector].empty?
          @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search] && params[:leader] && !params[:leader].empty?
          @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).search_by_leader(params[:leader]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search]
          @members = Member.paginate(page: params[:page], per_page: 50).search(params[:search]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        else
          @members = Member.all.includes(:sector, :leader).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc).sort_by{ |x| x.sector.id }.paginate(page: params[:page], per_page: 50)
        end
      }

      format.csv {

        if params[:search] && params[:sector] && params[:leader] && !params[:sector].empty? && !params[:leader].empty?
          @members = Member.search_by_leader(params[:leader]).search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search] && params[:sector] && !params[:sector].empty?
          @members = Member.search(params[:search]).search_by_sector(params[:sector]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search] && params[:leader] && !params[:leader].empty?
          @members = Member.search(params[:search]).search_by_leader(params[:leader]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        elsif params[:search]
          @members = Member.search(params[:search]).order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        else
          @members = Member.all.order(:sector_id, :leader_id, gender: :desc, is_leader: :desc)
        end
        send_data @members.to_csv
      }
      format.xls
    end

  end

  # GET /members/1
  # GET /members/1.json
  def show
  end

  # GET /members/new
  def new
    @member = Member.new
  end

  # GET /members/1/edit
  def edit
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to @member, notice: 'Member was successfully created.' }
        format.json { render :show, status: :created, location: @member }
      else
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      my_member_params = member_params
      my_member_params[:sector_id] = member_params[:sector_id].empty? ? @member.sector_id : member_params[:sector_id]
      if @member.sector_id != my_member_params[:sector_id].to_i && (@member.sector.users.pluck(:id).include?(current_user.id) || current_user.admin)
        @sector_transaction = SectorTransaction.create(previous_sector_id: @member.sector.id, current_sector_id: my_member_params[:sector_id], member: @member)
        UserNotifierMailer.send_notification(@member, @sector_transaction, Sector.find_by(id: my_member_params[:sector_id]).users.pluck(:email)).deliver
        my_member_params[:leader_id] = nil
      end
      if (@member.sector.users.pluck(:id).include?(current_user.id) || current_user.admin)  && @member.update(my_member_params)
        format.html { redirect_to @member, notice: 'Member was successfully updated.' }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { redirect_to @member, notice: "You are not authorized to edit #{@member.common_name}, please contact the Staff in charge" }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
      respond_to do |format|
        if (@member.sector && @member.sector.users.pluck(:id).include?(current_user.id) || current_user.admin) && @member.destroy
        format.html { redirect_to members_url, notice: 'Member was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { redirect_to members_path, notice: "You are not authorized to delete #{@member.common_name}, please contact the Staff in charge" }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  def member_group
    @member = Member.find(params[:member_id])
    @members = Member.includes(:sector)
  end

  def create_member_group
    @member = Member.find(params[:member_id])
    unless @member.member_groups.include?(@member)
      @member.member_groups << @member
    end
    redirect_to member_member_group_path(@member)
  end

  def add_member_group
    @member = Member.find(params[:member_id])
    @new_member = Member.find(params[:new_member_id])
    unless @member.member_groups.include?(@new_member)
      @member.member_groups << @new_member
    end
    redirect_to member_member_group_path(@member)
  end

  def delete_member_group
    @member = Member.find(params[:member_id])
    @mgi = @member.member_group_id
    @mm = Member.find(@mgi)
    @member.member_group_id = nil
    @member.save
    redirect_to member_member_group_path(@mm)
  end

  def import
    Member.import(params[:file])
    redirect_to members_path, notice: 'Data imported.'
  end

  def query_for_pledge_amount
    amount = nil
    if params[:id] && !params[:id].empty?
      amount = Member.query_for_pledge_amount(params[:id])
    end
    render json: { pledge_amount: amount }
  end

  def query_for_pledge_percentage
    amount = nil
    if params[:id] && !params[:id].empty?
      amount = Member.query_for_pledge_percentage(params[:id])
    end
    render json: { pledge_percentage: amount }
  end

  def query_for_pledge_building_fund
    amount = nil
    if params[:id] && !params[:id].empty?
      amount = Member.query_for_building_fund(params[:id])
    end
    render json: { pledge_building_fund: amount }
  end

  def query_for_pledge_special_contribution
    amount = nil
    if params[:id] && !params[:id].empty?
      amount = Member.query_for_special_contribution(params[:id])
    end
    render json: { pledge_special_contribution: amount }
  end

  def query_for_last_pledge
    pledge = nil
    if params[:id] && !params[:id].empty?
      pledge = Member.query_for_last_pledge(params[:id])
    end
    render json: { last_pledge: pledge }
  end

  def query_for_member_groups
    groups = nil
    if params[:id] && !params[:id].empty?
      member = Member.find(params[:id])
      groups = []
      if member.member_group_leader
        group_array = member.member_group_leader.member_groups
        group_array.each do |m|
          combine = {}
          combine["member"] = m
          combine["pledge"] = m.pledges.last
          groups.push(combine)
        end
      end
    end
    render json: { member_group: groups }
  end

  def query_for_contribution_transactions
    jan_contribution_transactions = []
    feb_contribution_transactions = []
    if params[:id] && !params[:id].empty?
      member = Member.find(params[:id])

      ## jan start
      jan_start = Date.new(Date.today.year, 1, 1)
      jan_end = jan_start.end_of_month
      jan_range = (jan_start..jan_end)
      jan_contribution_transactions = member.contribution_transactions.select{ |x| jan_range.cover?x.transaction_date}
      ## jan end

      ## jan start past year
      jan_start = Date.new(Date.today.year - 1, 1, 1)
      jan_end = jan_start.end_of_month
      jan_range = (jan_start..jan_end)
      jan_contribution_transactions_past_year = member.contribution_transactions.select{ |x| jan_range.cover?x.transaction_date}
      ## jan end


      ## feb start
      feb_start = Date.new(Date.today.year, 2, 1)
      feb_end = feb_start.end_of_month
      feb_range = (feb_start..feb_end)
      feb_contribution_transactions = member.contribution_transactions.select{ |x| feb_range.cover?x.transaction_date}

      feb_start = Date.new(Date.today.year - 1, 2, 1)
      feb_end = feb_start.end_of_month
      feb_range = (feb_start..feb_end)
      feb_contribution_transactions_past_year = member.contribution_transactions.select{ |x| feb_range.cover?x.transaction_date}
      ## feb end

      ## mar start
      mar_start = Date.new(Date.today.year, 3, 1)
      mar_end = mar_start.end_of_month
      mar_range = (mar_start..mar_end)
      mar_contribution_transactions = member.contribution_transactions.select{ |x| mar_range.cover?x.transaction_date}

      mar_start = Date.new(Date.today.year - 1, 3, 1)
      mar_end = mar_start.end_of_month
      mar_range = (mar_start..mar_end)
      mar_contribution_transactions_past_year = member.contribution_transactions.select{ |x| mar_range.cover?x.transaction_date}
      ## mar end

      ## apr start
      apr_start = Date.new(Date.today.year, 4, 1)
      apr_end = apr_start.end_of_month
      apr_range = (apr_start..apr_end)
      apr_contribution_transactions = member.contribution_transactions.select{ |x| apr_range.cover?x.transaction_date}

      apr_start = Date.new(Date.today.year - 1, 4, 1)
      apr_end = apr_start.end_of_month
      apr_range = (apr_start..apr_end)
      apr_contribution_transactions_past_year = member.contribution_transactions.select{ |x| apr_range.cover?x.transaction_date}
      ## apr end

      ## may start
      may_start = Date.new(Date.today.year, 5, 1)
      may_end = may_start.end_of_month
      may_range = (may_start..may_end)
      may_contribution_transactions = member.contribution_transactions.select{ |x| may_range.cover?x.transaction_date}

      may_start = Date.new(Date.today.year - 1, 5, 1)
      may_end = may_start.end_of_month
      may_range = (may_start..may_end)
      may_contribution_transactions_past_year = member.contribution_transactions.select{ |x| may_range.cover?x.transaction_date}
      ## may end

      ## june start
      june_start = Date.new(Date.today.year, 6, 1)
      june_end = june_start.end_of_month
      june_range = (june_start..june_end)
      june_contribution_transactions = member.contribution_transactions.select{ |x| june_range.cover?x.transaction_date}

      june_start = Date.new(Date.today.year - 1, 6, 1)
      june_end = june_start.end_of_month
      june_range = (june_start..june_end)
      june_contribution_transactions_past_year = member.contribution_transactions.select{ |x| june_range.cover?x.transaction_date}
      ## june end

      ## july start
      july_start = Date.new(Date.today.year, 7, 1)
      july_end = july_start.end_of_month
      july_range = (july_start..july_end)
      july_contribution_transactions = member.contribution_transactions.select{ |x| july_range.cover?x.transaction_date}

      july_start = Date.new(Date.today.year - 1, 7, 1)
      july_end = july_start.end_of_month
      july_range = (july_start..july_end)
      july_contribution_transactions_past_year = member.contribution_transactions.select{ |x| july_range.cover?x.transaction_date}
      ## july end

      ## aug start
      aug_start = Date.new(Date.today.year, 8, 1)
      aug_end = aug_start.end_of_month
      aug_range = (aug_start..aug_end)
      aug_contribution_transactions = member.contribution_transactions.select{ |x| aug_range.cover?x.transaction_date}

      aug_start = Date.new(Date.today.year - 1, 8, 1)
      aug_end = aug_start.end_of_month
      aug_range = (aug_start..aug_end)
      aug_contribution_transactions_past_year = member.contribution_transactions.select{ |x| aug_range.cover?x.transaction_date}
      ## aug end

      ## sep start
      sep_start = Date.new(Date.today.year, 9, 1)
      sep_end = sep_start.end_of_month
      sep_range = (sep_start..sep_end)
      sep_contribution_transactions = member.contribution_transactions.select{ |x| sep_range.cover?x.transaction_date}

      sep_start = Date.new(Date.today.year - 1, 9, 1)
      sep_end = sep_start.end_of_month
      sep_range = (sep_start..sep_end)
      sep_contribution_transactions_past_year = member.contribution_transactions.select{ |x| sep_range.cover?x.transaction_date}
      ## sep end

      ## oct start
      oct_start = Date.new(Date.today.year, 10, 1)
      oct_end = oct_start.end_of_month
      oct_range = (oct_start..oct_end)
      oct_contribution_transactions = member.contribution_transactions.select{ |x| oct_range.cover?x.transaction_date}

      oct_start = Date.new(Date.today.year - 1, 10, 1)
      oct_end = oct_start.end_of_month
      oct_range = (oct_start..oct_end)
      oct_contribution_transactions_past_year = member.contribution_transactions.select{ |x| oct_range.cover?x.transaction_date}
      ## oct end

      ## nov start
      nov_start = Date.new(Date.today.year, 11, 1)
      nov_end = nov_start.end_of_month
      nov_range = (nov_start..nov_end)
      nov_contribution_transactions = member.contribution_transactions.select{ |x| nov_range.cover?x.transaction_date}

      nov_start = Date.new(Date.today.year - 1, 11, 1)
      nov_end = nov_start.end_of_month
      nov_range = (nov_start..nov_end)
      nov_contribution_transactions_past_year = member.contribution_transactions.select{ |x| nov_range.cover?x.transaction_date}
      ## nov end

      ## dec start
      dec_start = Date.new(Date.today.year, 12, 1)
      dec_end = dec_start.end_of_month
      dec_range = (dec_start..dec_end)
      dec_contribution_transactions = member.contribution_transactions.select{ |x| dec_range.cover?x.transaction_date}

      last_dec_start = Date.new(Date.today.year - 1, 12, 1)
      last_dec_end = last_dec_start.end_of_month
      last_dec_range = (last_dec_start..last_dec_end)
      dec_contribution_transactions_past_year = member.contribution_transactions.select{ |x| last_dec_range.cover?x.transaction_date}
      ## dec end

      year_start = Date.new(Date.today.year, 12, 1).beginning_of_year
      year_end = dec_start.end_of_year
      year_range = (year_start..year_end)

      last_year_start = Date.new(Date.today.year - 1, 12, 1).beginning_of_year
      last_year_end = Date.new(Date.today.year - 1, 12, 1).end_of_year
      last_year_range = (last_year_start..last_year_end)

      special_cccom_transaction = member.contribution_transactions.select{ |x| year_range.cover?x.transaction_date }.select{ |x| x.fund_type === 'special_contribution' }.select{ |x| x.bank_account === 'CCCOM'}.map(&:amount).inject(0, &:+)

      special_cccom_transaction_past_year = member.contribution_transactions.select{ |x| last_year_range.cover?x.transaction_date }.select{ |x| x.fund_type === 'special_contribution' }.select{ |x| x.bank_account === 'CCCOM'}.map(&:amount).inject(0, &:+)

      special_tpri_transaction = member.contribution_transactions.select{ |x| year_range.cover?x.transaction_date }.select{ |x| x.fund_type === 'special_contribution' }.select{ |x| x.bank_account === 'TPRI'}.map(&:amount).inject(0, &:+)

      special_tpri_transaction_past_year = member.contribution_transactions.select{ |x| last_year_range.cover?x.transaction_date }.select{ |x| x.fund_type === 'special_contribution' }.select{ |x| x.bank_account === 'TPRI'}.map(&:amount).inject(0, &:+)
    end
    render json: {
      contribution_transactions: {
        jan: jan_contribution_transactions,
        janpastyear: jan_contribution_transactions_past_year,
        feb: feb_contribution_transactions,
        febpastyear: feb_contribution_transactions_past_year,
        mar: mar_contribution_transactions,
        marpastyear: mar_contribution_transactions_past_year,
        apr: apr_contribution_transactions,
        aprpastyear: apr_contribution_transactions_past_year,
        may: may_contribution_transactions,
        maypastyear: may_contribution_transactions_past_year,
        june: june_contribution_transactions,
        junepastyear: june_contribution_transactions_past_year,
        july: july_contribution_transactions,
        julypastyear: july_contribution_transactions_past_year,
        aug: aug_contribution_transactions,
        augpastyear: aug_contribution_transactions_past_year,
        sep: sep_contribution_transactions,
        seppastyear: sep_contribution_transactions_past_year,
        oct: oct_contribution_transactions,
        octpastyear: oct_contribution_transactions_past_year,
        nov: nov_contribution_transactions,
        novpastyear: nov_contribution_transactions_past_year,
        dec: dec_contribution_transactions,
        decpastyear: dec_contribution_transactions_past_year,
      },

      special_tpri_transaction: special_tpri_transaction,
      special_tpri_transaction_past_year: special_tpri_transaction_past_year,
      special_cccom_transaction: special_cccom_transaction,
      special_cccom_transaction_past_year: special_cccom_transaction_past_year,
      member: member.common_name
    }

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
        params.require(:member).permit(:ic_name, :common_name, :email, :ic_no, :d_o_b, :sector_id,
          :leader_id, :is_leader, :gender, :marital_status, :wedding_date, :race, :nationality,
          :previous_religion, :baptism_date, :first_invited_by, :highest_education, :graduated_from,
          :job_title, :company, :current_address_1, :current_address_2, :town, :postcode, :state,
          :house_telephone, :office_telephone, :handphone, :email, :hometown_address_1,
          :hometown_address_2, :ht_town, :ht_postcode, :ht_state, :hometown_telephone, :ht_country,
          :tpri, :receipt_name, :receipt_ic, :on_behalf, :esi, :notes)
    end
end
