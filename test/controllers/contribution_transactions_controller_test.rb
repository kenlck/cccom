require 'test_helper'

class ContributionTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contribution_transaction = contribution_transactions(:one)
  end

  test "should get index" do
    get contribution_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_contribution_transaction_url
    assert_response :success
  end

  test "should create contribution_transaction" do
    assert_difference('ContributionTransaction.count') do
      post contribution_transactions_url, params: { contribution_transaction: { amount: @contribution_transaction.amount, bank_statement_date: @contribution_transaction.bank_statement_date, transaction_date: @contribution_transaction.transaction_date, user_id: @contribution_transaction.user_id } }
    end

    assert_redirected_to contribution_transaction_url(ContributionTransaction.last)
  end

  test "should show contribution_transaction" do
    get contribution_transaction_url(@contribution_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_contribution_transaction_url(@contribution_transaction)
    assert_response :success
  end

  test "should update contribution_transaction" do
    patch contribution_transaction_url(@contribution_transaction), params: { contribution_transaction: { amount: @contribution_transaction.amount, bank_statement_date: @contribution_transaction.bank_statement_date, transaction_date: @contribution_transaction.transaction_date, user_id: @contribution_transaction.user_id } }
    assert_redirected_to contribution_transaction_url(@contribution_transaction)
  end

  test "should destroy contribution_transaction" do
    assert_difference('ContributionTransaction.count', -1) do
      delete contribution_transaction_url(@contribution_transaction)
    end

    assert_redirected_to contribution_transactions_url
  end
end
