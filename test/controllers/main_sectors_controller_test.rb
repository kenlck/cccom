require 'test_helper'

class MainSectorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @main_sector = main_sectors(:one)
  end

  test "should get index" do
    get main_sectors_url
    assert_response :success
  end

  test "should get new" do
    get new_main_sector_url
    assert_response :success
  end

  test "should create main_sector" do
    assert_difference('MainSector.count') do
      post main_sectors_url, params: { main_sector: { sector_name: @main_sector.sector_name, staff_id: @main_sector.staff_id } }
    end

    assert_redirected_to main_sector_url(MainSector.last)
  end

  test "should show main_sector" do
    get main_sector_url(@main_sector)
    assert_response :success
  end

  test "should get edit" do
    get edit_main_sector_url(@main_sector)
    assert_response :success
  end

  test "should update main_sector" do
    patch main_sector_url(@main_sector), params: { main_sector: { sector_name: @main_sector.sector_name, staff_id: @main_sector.staff_id } }
    assert_redirected_to main_sector_url(@main_sector)
  end

  test "should destroy main_sector" do
    assert_difference('MainSector.count', -1) do
      delete main_sector_url(@main_sector)
    end

    assert_redirected_to main_sectors_url
  end
end
