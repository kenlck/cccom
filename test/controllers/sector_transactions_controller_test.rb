require 'test_helper'

class SectorTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sector_transaction = sector_transactions(:one)
  end

  test "should get index" do
    get sector_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_sector_transaction_url
    assert_response :success
  end

  test "should create sector_transaction" do
    assert_difference('SectorTransaction.count') do
      post sector_transactions_url, params: { sector_transaction: {  } }
    end

    assert_redirected_to sector_transaction_url(SectorTransaction.last)
  end

  test "should show sector_transaction" do
    get sector_transaction_url(@sector_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_sector_transaction_url(@sector_transaction)
    assert_response :success
  end

  test "should update sector_transaction" do
    patch sector_transaction_url(@sector_transaction), params: { sector_transaction: {  } }
    assert_redirected_to sector_transaction_url(@sector_transaction)
  end

  test "should destroy sector_transaction" do
    assert_difference('SectorTransaction.count', -1) do
      delete sector_transaction_url(@sector_transaction)
    end

    assert_redirected_to sector_transactions_url
  end
end
