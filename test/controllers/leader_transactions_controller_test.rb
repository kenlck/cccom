require 'test_helper'

class LeaderTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leader_transaction = leader_transactions(:one)
  end

  test "should get index" do
    get leader_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_leader_transaction_url
    assert_response :success
  end

  test "should create leader_transaction" do
    assert_difference('LeaderTransaction.count') do
      post leader_transactions_url, params: { leader_transaction: {  } }
    end

    assert_redirected_to leader_transaction_url(LeaderTransaction.last)
  end

  test "should show leader_transaction" do
    get leader_transaction_url(@leader_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_leader_transaction_url(@leader_transaction)
    assert_response :success
  end

  test "should update leader_transaction" do
    patch leader_transaction_url(@leader_transaction), params: { leader_transaction: {  } }
    assert_redirected_to leader_transaction_url(@leader_transaction)
  end

  test "should destroy leader_transaction" do
    assert_difference('LeaderTransaction.count', -1) do
      delete leader_transaction_url(@leader_transaction)
    end

    assert_redirected_to leader_transactions_url
  end
end
