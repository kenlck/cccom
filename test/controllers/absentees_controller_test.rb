require 'test_helper'

class AbsenteesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @absentee = absentees(:one)
  end

  test "should get index" do
    get absentees_url
    assert_response :success
  end

  test "should get new" do
    get new_absentee_url
    assert_response :success
  end

  test "should create absentee" do
    assert_difference('Absentee.count') do
      post absentees_url, params: { absentee: { function_date: @absentee.function_date, member_id: @absentee.member_id, remarks: @absentee.remarks } }
    end

    assert_redirected_to absentee_url(Absentee.last)
  end

  test "should show absentee" do
    get absentee_url(@absentee)
    assert_response :success
  end

  test "should get edit" do
    get edit_absentee_url(@absentee)
    assert_response :success
  end

  test "should update absentee" do
    patch absentee_url(@absentee), params: { absentee: { function_date: @absentee.function_date, member_id: @absentee.member_id, remarks: @absentee.remarks } }
    assert_redirected_to absentee_url(@absentee)
  end

  test "should destroy absentee" do
    assert_difference('Absentee.count', -1) do
      delete absentee_url(@absentee)
    end

    assert_redirected_to absentees_url
  end
end
