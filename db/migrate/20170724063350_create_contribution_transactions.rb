class CreateContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :contribution_transactions do |t|
      t.date :transaction_date
      t.date :bank_statement_date
      t.float :amount
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
