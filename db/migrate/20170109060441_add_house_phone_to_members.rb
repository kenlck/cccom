class AddHousePhoneToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :house_telephone, :string
  end
end
