class AddMemberGroupToMembers < ActiveRecord::Migration[5.0]
  def change
    add_reference :members, :member_group, foreign_key: {to_table: :members}
  end
end
