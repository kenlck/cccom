class AddAmountPercentageToPledges < ActiveRecord::Migration[5.0]
  def change
    add_column :pledges, :amount_percentage, :string
  end
end
