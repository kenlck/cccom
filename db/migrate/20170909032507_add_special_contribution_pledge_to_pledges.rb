class AddSpecialContributionPledgeToPledges < ActiveRecord::Migration[5.0]
  def change
    add_column :pledges, :special_contribution_pledge, :float
  end
end
