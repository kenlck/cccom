class CreateSectorTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :sector_transactions do |t|
      t.references :previous_sector, foreign_key: {to_table: :sectors}
      t.references :current_sector, foreign_key: {to_table: :sectors}
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
