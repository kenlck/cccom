class AddExtraRelationToContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :contribution_transactions, :receipt_to, foreign_key: {to_table: :members}
    add_reference :contribution_transactions, :paid_by, foreign_key: {to_table: :members}
  end
end
