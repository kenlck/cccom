class AddMainSectorToSectors < ActiveRecord::Migration[5.0]
  def change
    add_reference :sectors, :main_sector, foreign_key: true
  end
end
