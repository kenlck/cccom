class AddReceiptNameToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :receipt_name, :string
  end
end
