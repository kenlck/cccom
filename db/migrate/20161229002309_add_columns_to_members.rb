class AddColumnsToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :postcode, :string
    add_column :members, :office_telephone, :string
    add_column :members, :handphone, :string
    add_column :members, :hometown_telephone, :string
  end
end
