class AddReceiptIcToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :receipt_ic, :string
  end
end
