class AddCommentToContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :contribution_transactions, :comment, :string
  end
end
