class AddOnBehalfToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :on_behalf, :boolean, default: false
  end
end
