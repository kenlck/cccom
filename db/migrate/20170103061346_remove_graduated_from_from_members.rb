class RemoveGraduatedFromFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :graduated_from, :string
  end
end
