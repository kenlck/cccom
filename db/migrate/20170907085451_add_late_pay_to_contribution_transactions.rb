class AddLatePayToContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :contribution_transactions, :late_pay, :boolean, default: false
  end
end
