class AddBuildingFundPledgeToPledges < ActiveRecord::Migration[5.0]
  def change
    add_column :pledges, :building_fund_pledge, :float
  end
end
