class AddSectorToContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    add_reference :contribution_transactions, :sector, foreign_key: true
  end
end
