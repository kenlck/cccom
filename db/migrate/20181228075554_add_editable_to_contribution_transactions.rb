class AddEditableToContributionTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :contribution_transactions, :editable, :boolean, default: true
  end
end
