class CreateMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :members do |t|
      t.string :ic_name
      t.string :common_name
      t.string :ic_no
      t.date :d_o_b
      t.string :gender
      t.string :marital_status
      t.date :wedding_date
      t.string :race
      t.string :nationality
      t.string :previous_religion
      t.date :baptism_date
      t.string :first_invited_by
      t.string :highest_education
      t.string :graduated_from
      t.string :job_title
      t.string :company
      t.string :current_address_1
      t.string :current_address_2
      t.string :town
      t.float :postcode
      t.string :state
      t.float :house_telephone
      t.float :office_telephone
      t.float :handphone
      t.string :email
      t.string :hometown_address_1
      t.string :hometown_address_2
      t.string :ht_town
      t.string :ht_postcode
      t.string :ht_state
      t.float :hometown_telephone
      t.string :ht_country
      t.string :leader
      t.string :leader_id
      t.string :sector_id

      t.timestamps
    end
  end
end
