class RemoveHousePhoneFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :house_telephone, :float
  end
end
