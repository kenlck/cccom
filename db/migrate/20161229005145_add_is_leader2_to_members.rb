class AddIsLeader2ToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :is_leader, :boolean, default: false
  end
end
