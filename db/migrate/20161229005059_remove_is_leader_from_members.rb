class RemoveIsLeaderFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :is_leader, :string
  end
end
