class AddIsLeaderToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :is_leader, :string
  end
end
