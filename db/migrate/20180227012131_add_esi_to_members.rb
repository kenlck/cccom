class AddEsiToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :esi, :boolean, default: false
  end
end
