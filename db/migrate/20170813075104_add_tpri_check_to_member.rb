class AddTpriCheckToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :tpri, :boolean, default: false
  end
end
