class AddPayableToSectors < ActiveRecord::Migration[5.0]
  def change
    add_column :sectors, :payable, :boolean, default: false
  end
end
