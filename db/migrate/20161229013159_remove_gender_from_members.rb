class RemoveGenderFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :gender, :string
  end
end
