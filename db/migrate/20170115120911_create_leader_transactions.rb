class CreateLeaderTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :leader_transactions do |t|
      t.references :member, foreign_key: true
      t.references :previous_leader, foreign_key: {to_table: :members}
      t.references :current_leader, foreign_key: {to_table: :members}
      t.timestamps
    end
  end
end
