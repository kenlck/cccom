class AddNewToSectors < ActiveRecord::Migration[5.0]
  def change
    add_column :sectors, :new, :boolean, default: false
  end
end
