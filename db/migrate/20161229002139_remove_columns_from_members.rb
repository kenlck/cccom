class RemoveColumnsFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :postcode, :float
    remove_column :members, :office_telephone, :float
    remove_column :members, :handphone, :float
    remove_column :members, :hometown_telephone, :float
  end
end
