class CreateSectorsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :sectors_users, :id => false do |t|
      t.references :sector, foreign_key: true
      t.references :user, foreign_key: true
    end
  end
end
