class CreateAbsentees < ActiveRecord::Migration[5.0]
  def change
    create_table :absentees do |t|
      t.references :member, foreign_key: true
      t.date :function_date
      t.string :remarks

      t.timestamps
    end
  end
end
