class AddDetailsToContributionTransaction < ActiveRecord::Migration[5.0]
  def change
    add_column :contribution_transactions, :bank_account, :string
    add_column :contribution_transactions, :fund_type, :string
  end
end
