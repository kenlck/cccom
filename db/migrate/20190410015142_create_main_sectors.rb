class CreateMainSectors < ActiveRecord::Migration[5.0]
  def change
    create_table :main_sectors do |t|
      t.string :sector_name
      t.references :staff, foreign_key: true

      t.timestamps
    end
  end
end
