class RemoveLeaderFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :leader, :string
  end
end
