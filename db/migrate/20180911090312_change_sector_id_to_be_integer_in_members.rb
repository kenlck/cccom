class ChangeSectorIdToBeIntegerInMembers < ActiveRecord::Migration[5.0]
  def change
    change_column :members, :sector_id, 'integer USING CAST(sector_id AS integer)'
  end
end
