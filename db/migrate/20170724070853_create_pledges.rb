class CreatePledges < ActiveRecord::Migration[5.0]
  def change
    create_table :pledges do |t|
      t.date :year
      t.float :amount
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
