# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Member.create(common_name: 'CName', ic_name: 'Name', ic_no: '911111-11-1111', d_o_b: '20/04/1990', marital_status: 'no', wedding_date: '20/04/1990', race: 'chinese', nationality: 'malaysian', previous_religion: 'hindu', baptism_date: '20/04/1990', first_invited_by: 'Ali', highest_education: 'Kindergarten', graduated_from: 'school', job_title: 'clerk', company: 'ytl', current_address_1: '85 jalan', current_address_2: 'Perak', town: 'Sitiawan', state: 'Perak', house_telephone: '1234', email: 'name@gmail.com', hometown_address_1: '84 Jalan', hometown_address_2: 'jalan 222', ht_town: 'Perak', ht_postcode: '32000', ht_state: 'Perak', ht_country: 'Japan', leader_id: '2', sector_id: '2', postcode: '46100', office_telephone: '6922222', handphone: '012-111 1111', hometown_telephone: '691 1111', is_leader: 1, gender: 1)
