# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190415115254) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "absentees", force: :cascade do |t|
    t.integer  "member_id"
    t.date     "function_date"
    t.string   "remarks"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["member_id"], name: "index_absentees_on_member_id", using: :btree
  end

  create_table "contribution_transactions", force: :cascade do |t|
    t.date     "transaction_date"
    t.date     "bank_statement_date"
    t.float    "amount"
    t.integer  "member_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "bank_account"
    t.string   "fund_type"
    t.integer  "receipt_to_id"
    t.integer  "paid_by_id"
    t.boolean  "late_pay",            default: false
    t.string   "comment"
    t.integer  "sector_id"
    t.boolean  "editable",            default: true
    t.index ["member_id"], name: "index_contribution_transactions_on_member_id", using: :btree
    t.index ["paid_by_id"], name: "index_contribution_transactions_on_paid_by_id", using: :btree
    t.index ["receipt_to_id"], name: "index_contribution_transactions_on_receipt_to_id", using: :btree
    t.index ["sector_id"], name: "index_contribution_transactions_on_sector_id", using: :btree
  end

  create_table "leader_transactions", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "previous_leader_id"
    t.integer  "current_leader_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["current_leader_id"], name: "index_leader_transactions_on_current_leader_id", using: :btree
    t.index ["member_id"], name: "index_leader_transactions_on_member_id", using: :btree
    t.index ["previous_leader_id"], name: "index_leader_transactions_on_previous_leader_id", using: :btree
  end

  create_table "main_sectors", force: :cascade do |t|
    t.string   "sector_name"
    t.integer  "staff_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["staff_id"], name: "index_main_sectors_on_staff_id", using: :btree
  end

  create_table "members", force: :cascade do |t|
    t.string   "ic_name"
    t.string   "common_name"
    t.string   "ic_no"
    t.date     "d_o_b"
    t.string   "marital_status"
    t.date     "wedding_date"
    t.string   "race"
    t.string   "nationality"
    t.string   "previous_religion"
    t.date     "baptism_date"
    t.string   "first_invited_by"
    t.string   "highest_education"
    t.string   "job_title"
    t.string   "company"
    t.string   "current_address_1"
    t.string   "current_address_2"
    t.string   "town"
    t.string   "state"
    t.string   "email"
    t.string   "hometown_address_1"
    t.string   "hometown_address_2"
    t.string   "ht_town"
    t.string   "ht_postcode"
    t.string   "ht_state"
    t.string   "ht_country"
    t.string   "leader_id"
    t.integer  "sector_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "postcode"
    t.string   "office_telephone"
    t.string   "handphone"
    t.string   "hometown_telephone"
    t.boolean  "is_leader",          default: false
    t.boolean  "gender"
    t.string   "house_telephone"
    t.boolean  "tpri",               default: false
    t.string   "receipt_name"
    t.string   "receipt_ic"
    t.boolean  "on_behalf",          default: false
    t.integer  "member_group_id"
    t.boolean  "esi",                default: false
    t.string   "notes"
    t.index ["member_group_id"], name: "index_members_on_member_group_id", using: :btree
  end

  create_table "pledges", force: :cascade do |t|
    t.date     "year"
    t.float    "amount"
    t.integer  "member_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.float    "building_fund_pledge"
    t.float    "special_contribution_pledge"
    t.string   "amount_percentage"
    t.index ["member_id"], name: "index_pledges_on_member_id", using: :btree
  end

  create_table "sector_transactions", force: :cascade do |t|
    t.integer  "previous_sector_id"
    t.integer  "current_sector_id"
    t.integer  "member_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["current_sector_id"], name: "index_sector_transactions_on_current_sector_id", using: :btree
    t.index ["member_id"], name: "index_sector_transactions_on_member_id", using: :btree
    t.index ["previous_sector_id"], name: "index_sector_transactions_on_previous_sector_id", using: :btree
  end

  create_table "sectors", force: :cascade do |t|
    t.string   "sector_type"
    t.integer  "staff_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id"
    t.boolean  "active",         default: true
    t.boolean  "new",            default: false
    t.boolean  "payable",        default: false
    t.integer  "main_sector_id"
    t.index ["main_sector_id"], name: "index_sectors_on_main_sector_id", using: :btree
    t.index ["staff_id"], name: "index_sectors_on_staff_id", using: :btree
    t.index ["user_id"], name: "index_sectors_on_user_id", using: :btree
  end

  create_table "sectors_users", id: false, force: :cascade do |t|
    t.integer "sector_id"
    t.integer "user_id"
    t.index ["sector_id"], name: "index_sectors_users_on_sector_id", using: :btree
    t.index ["user_id"], name: "index_sectors_users_on_user_id", using: :btree
  end

  create_table "staffs", force: :cascade do |t|
    t.string   "common_name"
    t.string   "ic_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.boolean  "boss",                   default: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "absentees", "members"
  add_foreign_key "contribution_transactions", "members"
  add_foreign_key "contribution_transactions", "members", column: "paid_by_id"
  add_foreign_key "contribution_transactions", "members", column: "receipt_to_id"
  add_foreign_key "contribution_transactions", "sectors"
  add_foreign_key "leader_transactions", "members"
  add_foreign_key "leader_transactions", "members", column: "current_leader_id"
  add_foreign_key "leader_transactions", "members", column: "previous_leader_id"
  add_foreign_key "main_sectors", "staffs"
  add_foreign_key "members", "members", column: "member_group_id"
  add_foreign_key "pledges", "members"
  add_foreign_key "sector_transactions", "members"
  add_foreign_key "sector_transactions", "sectors", column: "current_sector_id"
  add_foreign_key "sector_transactions", "sectors", column: "previous_sector_id"
  add_foreign_key "sectors", "main_sectors"
  add_foreign_key "sectors", "staffs"
  add_foreign_key "sectors", "users"
  add_foreign_key "sectors_users", "sectors"
  add_foreign_key "sectors_users", "users"
end
