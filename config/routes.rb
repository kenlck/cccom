Rails.application.routes.draw do

  resources :main_sectors
  resources :absentees
  resources :pledges
  resources :contribution_transactions
  devise_for :users
  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'members#new'
  get  '/members/pledge_amount_query',  to: 'members#query_for_pledge_amount', as: :pledge_amount_query
  get  '/members/pledge_amount_percentage_query',  to: 'members#query_for_pledge_percentage', as: :pledge_amount_percentage_query
  get  '/members/pledge_building_fund_pledge_query',  to: 'members#query_for_pledge_building_fund', as: :pledge_building_fund_pledge_query
  get  '/members/pledge_special_contribution_pledge_query',  to: 'members#query_for_pledge_special_contribution', as: :pledge_special_contribution_pledge_query
  get  '/members/pledge_last_pledge_query',  to: 'members#query_for_last_pledge', as: :pledge_last_pledge_query
  get  '/members/contribution_transactions',  to: 'members#query_for_contribution_transactions', as: :contribution_transactions_query
  get  '/members/member_groups',  to: 'members#query_for_member_groups', as: :member_groups_query
  get  '/members/indexall', to: 'members#indexall'
  get  '/contribution_transactions_member', to: 'contribution_transactions#member'
  get  '/my_contribution_transactions_member', to: 'contribution_transactions#member_records'
  get  '/contribution_transactions_summary', to: 'contribution_transactions#summary'
  get  '/contribution_transactions_summary2', to: 'contribution_transactions#summary2'
  get  '/contribution_transactions_summary3', to: 'contribution_transactions#summary3'
  get  '/contribution_transactions_summary4', to: 'contribution_transactions#summary4'
  get  '/contribution_transactions_bank_summary', to: 'contribution_transactions#bank_summary'
  get  '/contribution_transactions_tpri', to: 'contribution_transactions#tpri'
  get  '/pledges_member', to: 'pledges#member'
  get  '/absent_report', to: 'absentees#absent_report'

  resources :members do
    get :member_group
    get :create_member_group
    get :add_member_group
    get :delete_member_group
    collection do
      post :import
    end
  end
  resources :staffs
  resources :sectors
  resources :sector_transactions

  devise_scope :user do
     get 'logout', :to => 'devise/sessions#destroy'
  end
end
